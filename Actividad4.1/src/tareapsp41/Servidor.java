/*
 * El servidor generará un número aleatoria, y abrirá el puerto 2000 al que 
 * permanecerá en escucha esperando la conexión de un equipo cliente y acierte
 * el número generado.
 */

package tareapsp41;

import java.io.*;
import java.net.*;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Servidor {
	
    private static final int PUERTO= 2000;

	public static void main(String[] args) {
		int sesionId;
		ServerSocket socketServidor;
		Socket sock;
		
		try {
			socketServidor = new ServerSocket(PUERTO);
			sesionId=0;
			
			while(true) {
				sock=socketServidor.accept();
				new HiloServidor(sock, sesionId).start();
				sesionId++;
			}
		}catch (IOException e) {
			
		}
	}

}

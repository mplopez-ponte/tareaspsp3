/* 
* El cliente se conectará a través del puerto 2000 a un servidor, el cual intentará
* adivinar el número aleatorio creado por el propio server enviando distintos números
* números y según las pistas que el server vaya facilitando hasta que el número sea
* acertado.
*/

package tareapsp41;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Cliente {
	
	public static final int PUERTO=2000;
    static final String HOST="localhost";
    String numElegido;
    String numResult;
    
    //Creamos el constructor de la clase.
    public Cliente() {
    	try {
    		//Conectamos el servidor a través del puerto 2000
    		Socket socketCliente = new Socket(HOST, PUERTO);
    		
    		do {
    			//Vamos a solicitar al cliente que inrozca un número
    			//y lo envíe al servidor
    			numElegido=JOptionPane.showInputDialog("Introduzca un número entre el 0 y el 100: ");
    			//Vamos a abrir el flujo de datos y enviarlos al servidor
    			OutputStream os_salida=socketCliente.getOutputStream();
    			DataOutputStream salida_flujo = new DataOutputStream(os_salida);
    			//Enviamos el número al servidor
    			salida_flujo.writeUTF(numElegido);
    			
    			//Vamos a leer el resultado que nos envía el servidor y se indica
    			//si hemos acertado el número
    			InputStream is_entrada = socketCliente.getInputStream();
    			DataInputStream entrada_flujo = new DataInputStream(is_entrada);
    			//A continuación guardaremos el resultado en una variable
    			numResult=entrada_flujo.readUTF(entrada_flujo);
    			
    			if(numResult.equalsIgnoreCase("Más alto...")||numResult.equalsIgnoreCase("Más bajo...")){
    	            System.out.println(numResult);
    	        }
    	        }while (!"OK".equals(numResult));
    		
    		JOptionPane.showMessageDialog(null, "Felicidades! Has acertado el número!");
    		//Cerramos el puerto de comunicación con el servidor
    		socketCliente.close();
    		
    		//Vamos a capturar las excepciones y las mostramos poir pantalla
    		
    		}catch (Exception e) {
    			System.out.println(e.getMessage());
    	}
    }

	public static void main(String[] args) {
		// Vamos a crear el cliente
		new Cliente();
	}

}

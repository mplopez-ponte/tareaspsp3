package tareapsp41;

import java.io.*;
import java.net.*;
import java.util.logging.*;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class HiloServidor extends Thread {
	
	private Socket socketCliente;
	private int sesionId;
	private int numAleatorio;
	private DataInputStream entrada_flujo;
	private DataOutputStream salida_flujo;
	
	// Creamos el constructor de la clase
	public HiloServidor (Socket socketCliente, int sesionId) {
		this.socketCliente=socketCliente;
		this.sesionId=sesionId;
	}
	
	public void run() {
		String numResult = null;
		// Vamos a crear un número de forma aleatoria que el Cliente
		// debe adivinar
		numAleatorio=(int)(Math.random()*100);
		
		 do{
	            InputStream is_entrada=null;
	                try {
	                    
	                	//Se comprueba que el número que envía el Cliente es mayor, menor o si el mismo
	                	//que el Servidor generó.
	                    is_entrada = socketCliente.getInputStream();
	                    DataInputStream entrada_flujo=new DataInputStream(is_entrada);
	                    numResult=entrada_flujo.readUTF();
	                    //Se comparan los resultados para comprobar si el número es menor
	                    if(Integer.parseInt(numResult)>numAleatorio){
	                        OutputStream os_salida=socketCliente.getOutputStream();
	                        DataOutputStream salida_flujo=new DataOutputStream(os_salida);
	                        salida_flujo.writeUTF("Más bajo...");
	                        
	                    //Se comparan los resultados para comprobar si el número es mayor    
	                    } else if (Integer.parseInt(numResult)<numAleatorio){
	                        OutputStream os_salida=socketCliente.getOutputStream();
	                        DataOutputStream salida_flujo=new DataOutputStream(os_salida);
	                        salida_flujo.writeUTF("Más alto...");
	                        
	                    }else if (Integer.parseInt(numResult)==numAleatorio){
	                        OutputStream os_salida=socketCliente.getOutputStream();
	                        DataOutputStream salida_flujo=new DataOutputStream(os_salida);
	                        salida_flujo.writeUTF("OK");
	                    }   } catch (IOException ex) {
	                    Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
	                } 
	            }while(numAleatorio!=Integer.parseInt(numResult));
	            
	            //cerrarHilo();
	    }
	    
	    //Este método cierra la conexión para cualquier cliente
	    public void cerrarHilo(){
	        
	        if (entrada_flujo !=null){
	            try{
	                entrada_flujo.close();
	            }catch (IOException e){
	                System.err.println("Error al cerrar el flujo de entrada");
	                System.err.println(e.toString());
	            }
	        } else if (salida_flujo !=null){
	            try {
	                salida_flujo.close();
	            }catch (Exception e1){
	                System.err.println("Error al cerrar el flujo de salida");
	                System.err.println(e1.toString());
	            }
	        }
	    }
	    
	}

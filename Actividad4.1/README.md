## Tarea para PSP04
### Actividad 4.1. Modifica el ejercicio 1 de la unidad 3 para el servidor permita trabajar de forma concurrente con varios clientes.

Lo primero es necesario arrancar la parte del Servidor, a continuación se arranca la parte del Cliente que es la que vaya a preguntar el número antes 
de ser enviada al servidor.

![Ventana en la que se pregunta al cliente](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/La%20ventana%20pregunta%20el%20n%C3%BAmero.png)

La respuesta del servidor al cliente:

![Mostrando la respuesta del servidor](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/La%20respuesta%20del%20servidor.png)

Una vez cuando se acierte el número, aparece una ventana en la que se informa que el número ha sido acertado, tal como se muestra en la siguiente imagen:

![Mensaje de acierto](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Mensaje%20de%20acierto.png)

## Tarea para PSP04.
### Actividad 4.2. Modifica el ejercicio 2 de la unidad 3 para el servidor permita trabajar de forma concurrente con varios clientes.

Lo primero que debemos hacer es arrancar el servidor para que quede a la escucha y se pueda conectar el cliente.

A continuación ejecutamos la parte del cliente, apareciendo un cuadro de diálogo que pregunta el nombre del fichero, tal como se muestra
la siguiente imagen:

![Cuadro de diálogo](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Ejercicio4.2_Captura.PNG)

En caso de que el fichero solicitado no exista, se nos mostrará un mensaje informando del problema:

![Si el fichero no existe](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Ejercicio4.2_Captura2.PNG)

Vamos ahora a suponer que el fichero existe, pero para ello debemos introducir la ruta completa al archivo ejemplo.txt

![Si el fichero existe](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Captura3.PNG)

El fichero está almacenado en la localización /src/ejemplo.txt tal como se va a mostrar en la imagen:

![El fichero ejemplo.txt](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Captura4.PNG)
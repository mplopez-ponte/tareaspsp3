package tareapsp42;

import java.io.*;
import java.net.*;
import java.util.logging.*;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class HiloServidor extends Thread {
	
	private Socket sockCliente;
	private int sesionId;
	private String nomFichero;
    private File sFichero=new File("\\src\\"+nomFichero);
    private String textoFichero;
    private DataOutputStream salida_flujo;
    private DataInputStream entrada_flujo;
    
    public HiloServidor(Socket sCliente, int idSesion){
        this.sockCliente=sCliente;
        this.sesionId=idSesion;
    }
    
    //Este método se ejecutará cuando se crea un hilo de la clase HiloServidor
    public void run() {
    	InputStream is_entrada=null;
    	try {
    		String nomFichero;
    		//Vamos a crear un flujo de entrada por donde el servidor recibirá las solicitudes
    		is_entrada = sockCliente.getInputStream();
    		entrada_flujo=new DataInputStream(is_entrada);
            //Leemos la solicitud y guardamos el nombre del fichero solicitado en una variable
            nomFichero=entrada_flujo.readUTF();
            //Buscamos por el nombre del fichero si existe o no.
            if(sFichero.exists()){
                //Abrimos los flujos de salida para enviar el fichero al cliente
                OutputStream os_salida=sockCliente.getOutputStream();
                salida_flujo=new DataOutputStream(os_salida);
                
                //Creamos un buffer que gestione el envío del fichero
                FileReader lector=new FileReader(sFichero);
                BufferedReader bf=new BufferedReader(lector);
                //Mientras haya datos en el fichero los envía al cliente
                while((textoFichero=bf.readLine())!=null){
                    salida_flujo.writeUTF(textoFichero);
                }
                System.out.println("Fichero enviado...");
                
            }else {
                OutputStream os_salida=sockCliente.getOutputStream();
                DataOutputStream salida_flujo=new DataOutputStream(os_salida);
                salida_flujo.writeUTF("El fichero solicitado no existe.");
                
            }   } catch (IOException ex) {
            Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
        } 
        //cerrarHilo();
    }
    
    //Método para terminar el hilo cuando ya no se necesita.
    public void cerrarHilo(){
        if(salida_flujo !=null){
            try {
                salida_flujo.close();
            } catch (IOException ex) {
                Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if(entrada_flujo !=null){
            try{
                entrada_flujo.close();
            }catch(IOException ex){
                Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
    		
    	
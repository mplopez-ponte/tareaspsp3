/*
 * El cliente se va a conectar a un servidor solicitando el envío de un fichero
 * cuyo nombre es facilitado por el usuario. Si el fichero existe, el servidor
 * se lo envía y el cliente lo muestra.
 */

package tareapsp42;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Cliente {
	
	static final int PUERTO=1500;
	static final String HOST="localhost";
	String nomFichero;
	String request;
	public Cliente() {
		try {
			//Se crea la conexión con el servidor
			Socket sockCliente = new Socket(HOST, PUERTO);
			//Se abre el flujo de salida
			OutputStream os_salida = sockCliente.getOutputStream();
			DataOutputStream salida_flujo = new DataOutputStream(os_salida);
			
			//El usuario introducirá el nombre del fichero que le solicitará al servidor
			 do{
	                nomFichero=JOptionPane.showInputDialog(
	                    "Introduzca el nombre completo del fichero");
	            salida_flujo.writeUTF(nomFichero);
	            
	            //Comprobamos si existe o está bien escrito y si no es así se pide de
	            // nuevo al usuario que introduzca bien el nombre del fichero.
	            InputStream is_entrada=sockCliente.getInputStream();
	            DataInputStream entrada_flujo=new DataInputStream(is_entrada);
	            request=entrada_flujo.readUTF();
	            if (request.equalsIgnoreCase("El fichero solicitado no existe.")){
	              System.out.println("El fichero solicitado no existe, escriba "
	                        + "el nombre del archivo correctamente");
	            }
	            }while(request.equalsIgnoreCase("El fichero solicitado no existe."));
	            
	            //Se recibe el fichero que hemos solicitado y se muestra por pantalla
	            InputStream is_entrada=sockCliente.getInputStream();
	            DataInputStream entrada_flujo=new DataInputStream(is_entrada);
	            System.out.println(entrada_flujo.readUTF());
	            
	            //Cerramos la conexión con el servidor
	            sockCliente.close();
	            
	        }catch(Exception e){
	            
	            System.out.println(e.getMessage());
	        }
	    }
	    public static void main(String[] args) {
	        // Creamos un cliente
	        new Cliente();
	    }
	    
	}

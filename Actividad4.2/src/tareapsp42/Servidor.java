package tareapsp42;

import java.io.*;
import java.net.*;

/**
 *
 * @author María Paz López Ponte
 */
public class Servidor {
    
    public static void main(String[] args) throws IOException {
        ServerSocket sockServidor;
        Socket sockCliente;
        final int PUERTO=1500;
        int sesionId;
        
        try{
        //Vamos a crear la conexión
        sockServidor=new ServerSocket(PUERTO);
        sesionId=0;
        
        while(true){
            
        	//Vamos a esperar a que el cliente se vaya a conectar por el puerto 1500
            sockCliente=sockServidor.accept();
            new HiloServidor(sockCliente, sesionId).start();
            sesionId++;
        }
        }catch(IOException ex){
            
        } 
    }
    
}
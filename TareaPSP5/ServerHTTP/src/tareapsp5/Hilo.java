package tareapsp5;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author friki
 */
public class Hilo extends Thread{
    private Socket socketCliente;

    public Hilo(Socket socketCliente) {

        this.socketCliente = socketCliente;

}
    
    
    public void run() {

    try{
      
        ServidorHTTP.procesaPeticion(socketCliente);
        //cierra la conexión entrante
        socketCliente.close();
        System.out.println("cliente atendido");
          
    } catch (IOException ex) {
        ex.printStackTrace();
    }
    
  }

}

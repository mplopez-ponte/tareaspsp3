### Tarea para PSP05.

## Ejercicio 1.
Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que incluya la cabecera Date.

## Ejercicio 2.
Modifica el ejemplo del servidor HTTP (Proyecto java ServerHTTP, apartado 5.1 de los contenidos) para que implemente multihilo, y pueda gestionar la concurrencia de manera eficiente.

Una vez ya ejecutamos la aplicación y arrancamos nuestro servidor HTTP, se nos muestra las distintas URL que conforma nuestro servidor tal como se muestra en la siguiente imagen:

![Ejecutando la aplicacion](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/TareaPSP5_Ejecutando%20la%20aplicaci%C3%B3n.PNG)


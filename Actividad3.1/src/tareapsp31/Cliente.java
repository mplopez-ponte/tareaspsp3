/*
 * Este cliente se conecta por el puerto 2000 a un servidor, e intenta adivinar
 * el número creado por el mismo enviando distintos posibles números y atendiendo
 * a las pistas que el servidor facilita hasta que el número es adivinado.
 */
package tareapsp31;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 *
 * @author María Paz López Ponte
 */
public class Cliente {

    public static final int PUERTO=2000;
    static final String HOST="localhost";
    String numElegido;
    String numResult;
    
    
    //Constructor de la clase.
    public Cliente(){
        
        try{
        //Conexión al servidor por el puerto 2000    
        Socket socketCliente=new Socket(HOST, PUERTO);
        
        do{
        //Solicitamos al cliente que introduzca un número y lo envíe al servidor
        numElegido=JOptionPane.showInputDialog("Introduzca un número entre el 0 y el 100: ");
        //Abrimos el flujo de datos para enviarlos al sevidor
        OutputStream os_salida=socketCliente.getOutputStream();
        DataOutputStream flujoSalida=new DataOutputStream(os_salida);
        //Enviamos el número al servidor
        flujoSalida.writeUTF(numElegido);
        
        //Leemos el resultado que nos envía el servidor y nos indica si hemos acertado el número
        InputStream is_entrada=socketCliente.getInputStream();
        DataInputStream flujoEntrada=new DataInputStream(is_entrada);
        //Guardamos en una variable el resultado
        numResult=DataInputStream.readUTF(flujoEntrada);
        
        if(numResult.equalsIgnoreCase("Más alto...")||numResult.equalsIgnoreCase("Más bajo...")){
            System.out.println(numResult);
        }
        }while (!"OK".equals(numResult));
        
        System.out.println("Felicidades! Has acertado el número!");
        
        //Se cierra el puerto de comunicación con el servidor
        socketCliente.close();
        
     //Capturamos las excepciones y las mostramos por pantalla.   
    }catch (Exception e){
        
        System.out.println(e.getMessage());
    }
    }
    
    public static void main(String[] args) {
        // Creamos un cliente
        new Cliente();
    }
    
}
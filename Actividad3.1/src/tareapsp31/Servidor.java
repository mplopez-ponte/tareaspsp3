/* El servidor genera un número aleatorio, abre el puerto 2000 
 * y permanece a la escucha en espera de conexión a un equipo cliente 
 * que se conectará al mismo y adivine el número generado entre 0 y 100.
 */

package tareapsp31;

import java.io.*;
import java.net.*;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Servidor {

    static final int PUERTO= 2000;
    int numAleatorio;
    String numResult;
    
    public Servidor(){
        
        try{
            //Iniciamos la escucha del servidor por el puerto 2000
            ServerSocket socketServidor= new ServerSocket(PUERTO);
            //Creamos socket para el cliente y esperamos a que responda por dicho puerto
            Socket socketCliente=socketServidor.accept();
            
            //Creamos un número de forma aleatoria que el Cliente debe adivinar
            numAleatorio=(int)(Math.random()*100);
            
            do{
            //Comprobamos si el número que nos envía el Cliente es mayor, menor o el mismo
            //que ha creado aleatoriamente el Servidor.
            InputStream  is_entrada=socketCliente.getInputStream();
            DataInputStream flujoEntrada=new DataInputStream(is_entrada);
            numResult=flujoEntrada.readUTF();
            
            //Comparamos los resultados y actuamos según el mismo
            if(Integer.parseInt(numResult)>numAleatorio){
                OutputStream os_salida=socketCliente.getOutputStream();
                DataOutputStream flujoSalida=new DataOutputStream(os_salida);
                flujoSalida.writeUTF("Más bajo...");
                
            } else if (Integer.parseInt(numResult)<numAleatorio){
                OutputStream os_salida=socketCliente.getOutputStream();
                DataOutputStream flujoSalida=new DataOutputStream(os_salida);
                flujoSalida.writeUTF("Más alto...");
                
            }else if (Integer.parseInt(numResult)==numAleatorio){
                OutputStream os_salida=socketCliente.getOutputStream();
                DataOutputStream flujoSalida=new DataOutputStream(os_salida);
                flujoSalida.writeUTF("OK");
            }
            }while(numAleatorio!=Integer.parseInt(numResult));
            //Cerramos la conexión
            socketServidor.close();
            
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String[] args) {
        //Creamos un servidor
        
        new Servidor();
    }
    
}
# Tarea para PSP03
## Actividad 3.1
**Actividad 3.1. El objetivo del ejercicio es crear una aplicación cliente/servidor que se comunique por el puerto 2000 y realice lo siguiente:
El servidor debe generar un número secreto de forma aleatoria entre el 0 al 100. El objetivo de cliente es solicitarle al usuario un número y enviarlo al servidor hasta que adivine el número secreto. Para ello, el servidor para cada número que le envía el cliente le indicará si es menor, mayor o es el número secreto del servidor.**

Lo primero que debemos hacer es ejecutar la clase Servidor para levantar el servicio y que el cliente pueda realizar la conexión de forma correcta.

![Servidor arrancado](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Servidor%20Arrancado.jpg)

Una vez ya disponemos de la clase Servidor en funcionamiento, ya podemos ejecutar la parte cliente, y que en este caso nos aparecerá un cuadro de diálogo que nos solicita la entrada de un número entre 0 y 100.

![Introduciendo el número](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Introducimos%20un%20n%C3%BAmero.jpg)

Según el número introducido y según el servidor haya generado el número aleatoriamente, los mensajes que van a aparecen en la consola serán los siguiente:

![Respuesta del servidor en el cliente](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Ejecutamos%20el%20cliente.png)

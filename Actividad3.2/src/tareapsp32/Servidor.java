/*
 * Actividad 3.2. 
 * El objetivo del ejercicio es crear una aplicación cliente/servidor 
 * que permita el envío de ficheros al cliente. 
 * Para ello, el cliente se conectará al servidor por el puerto 1500 
 * y le solicitará el nombre de un fichero del servidor. 
* Si el fichero existe, el servidor, le enviará el fichero al cliente 
* y éste lo mostrará por pantalla. 
* Si el fichero no existe, el servidor le enviará al cliente un mensaje de error. 
* Una vez que el cliente ha mostrado el fichero se finalizará la conexión.
*/

package tareapsp32;

import java.io.*;
import java.net.*;

/**
 *
 * @author María Paz López Ponte
*/

public class Servidor {
	
	static final int PUERTO = 1500;
	String nomFichero;
	File sFichero = new File("\\src\\" + nomFichero);
	String textoFichero;


public Servidor() throws IOException {
	
	try {
		//Creamos la conexión
		ServerSocket socketServidor = new ServerSocket(PUERTO);
		//Esperamos a que el cliente se pueda conectar por el puerto
		Socket sCliente = socketServidor.accept();
		
		//Es el momento para crear un flujo de entrada por donde el servidor 
		//va a recibir las solicitudes
		InputStream entrada = sCliente.getInputStream();
		DataInputStream flujoEntrada = new DataInputStream(entrada);
		//Leemos la solicitud y se guarda el nombre del fichero en una variable
		nomFichero = flujoEntrada.readLine();
		
		//Vamos a buscar por el nombre del fichero si existe o no.
		if(sFichero.exists()) {
			//Vamos a abrir los flujos de salida para envio del fichero al cliente
			OutputStream salida = sCliente.getOutputStream();
			DataOutputStream flujoSalida = new DataOutputStream(salida);
			
			//Vamos a crear un buffer que vaya a gestionar el envío del fichero
			FileReader lector = new FileReader(sFichero);
			BufferedReader bf = new BufferedReader(lector);
			//Los datos del fichero se envían al cliente, siempre que haya datos.
			while((textoFichero = bf.readLine())!=null) {
				flujoSalida.writeUTF(textoFichero);
			}
			
			System.out.println("Fichero enviado...");
			
			//Vamos a cerrar los flujos de datos
			bf.close();
			flujoSalida.close();
			salida.close();
		} else {
			OutputStream salida = sCliente.getOutputStream();
			DataOutputStream flujoSalida = new DataOutputStream(salida);
			flujoSalida.writeUTF("El fichero solicitado no existe.");
			//Se cierran los flujos de salida
			flujoSalida.close();
			salida.close();
		}
	}catch (Exception e) {
		System.out.println(e.getMessage());
	}
	
}

public static void main(String[] args) throws IOException {
	//Vamos a crear un servidor
	new Servidor();
	
	//Se crea un archivo en el directorio para que pueda mostrarse
	FileWriter fichero = new FileWriter("\\src\\ejemplo.txt");
	fichero.write("Esto es un texto de ejemplo para la Actividad 3.2 de PSP");
	fichero.close();
}
}
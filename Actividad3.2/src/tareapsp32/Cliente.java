/*
 * El cliente se conectará al servidor por el puerto 1500 
 * y le solicitará el nombre de un fichero del servidor.
 * Si el fichero existe, el servidor, le enviará el fichero 
 * al cliente y éste lo mostrará por pantalla.
*/

package tareapsp32;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 *
 *@author María Paz López Ponte
*/

public class Cliente {
	static final int PUERTO = 1500;
	static final String HOST = "localhost";
	String nomFichero;
	String respuesta;
	public Cliente() {
		try {
			//Vamos a crear la conexión con el servidor 
			Socket socketCliente = new Socket (HOST, PUERTO);
			//Se abre un flujo de salida
			OutputStream os_salida = socketCliente.getOutputStream();
			DataOutputStream flujoSalida = new DataOutputStream(os_salida);
			
			//El usuario va a introducir el nombre del fichero que le 
			//solicita al servidor
			do {
				nomFichero = JOptionPane.showInputDialog(
						"Introduzca el nombre completo del fichero");
				flujoSalida.writeUTF(nomFichero);
				
				//Comprobamos si existe o está bien escrito y si no es así 
				//se pide que el usuario introduzca de nuevo el nombre bien escrito.
				InputStream entrada = socketCliente.getInputStream();
				DataInputStream flujoEntrada = new DataInputStream(entrada);
				respuesta = flujoEntrada.readUTF();
				if (respuesta.equalsIgnoreCase("El fichero solicitado no existe.")) {
					JOptionPane.showMessageDialog(null, "El fichero solicitado no existe, escriba "
							+ "el nombre del archivo correctamente");
				}
				}while (respuesta.equalsIgnoreCase("El fichero solicitado no existe"));
				
				//Recibimos el fichero solicitado y lo mostramos por pantalla
				InputStream entrada = socketCliente.getInputStream();
				DataInputStream flujoEntrada = new DataInputStream(entrada);
				System.out.println(flujoEntrada.readUTF());
				
				//Cerramos la conexión con el servidor
				socketCliente.close();
				
		}catch(Exception e) {
			
			System.out.println(e.getMessage());
			
		}
	}
	
	public static void main (String[] args) {
		//Se crea un cliente
		new Cliente();
	}
	
}
# Tarea para PSP03
## Actividad 3.2
**Actividad 3.2.  El objetivo del ejercicio es crear una aplicación cliente/servidor que permita el envío de ficheros al cliente. Para ello, el cliente se conectará al servidor por el puerto 1500 y le solicitará el nombre de un fichero del servidor. Si el fichero existe, el servidor, le enviará el fichero al cliente y éste lo mostrará por pantalla. Si el fichero no existe, el servidor le enviará al cliente un mensaje de error. Una vez que el cliente ha mostrado el fichero se finalizará la conexión.**

Lo primero que debemos hacer es ejecutar la clase Servidor para levantar el servicio y que el cliente pueda realizar la conexión de forma correcta.

Una vez ya tenemos el Servidor arrancado, pasamos a ejecutar la clase Cliente, apareciendo un cuadro de diálogo en el que se nos solicita el nombre completo del fichero:

![Preguntando el nombre del fichero](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Introduciendo_nombrefichero.png)

Si el fichero no existe, se nos muestra una ventana con un mensaje que informa que el fichero no existe, tal como se muestra en la siguiente imagen_

![Mostrando el mensaje](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Mensaje_noexistefichero.png)

En este caso, el fichero si existe, y lo podemos comprobar dentro de la carpeta del proyecto, se muestra en esta imagen:

![Mostrando el fichero creado](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Carpeta%20con%20el%20fichero%20creado.png)

Por último, vamos a comprobar que se ha podido escribir en el fichero, tal como se muestra en la siguiente imagen:

![Mostrando el fichero con contenido](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Se%20ha%20conseguido%20escribir%20dentro%20del%20fichero.png)

## Tarea para PSP04.
### Actividad 4.3. A partir del ejercicio anterior crea un servidor que una vez iniciada sesión a través de un nombre de usuario y contraseña específico (por ejemplo javier / secreta) el sistema permita Ver el contenido del directorio actual, mostrar el contenido de un determinado archivo y salir.

Lo primero que debemos hacer, es arrancar la parte del servidor. A continuación se arranca la parte del cliente, que nos va a solicitar el usuario y contraseña para poder realizar las operaciones necesarias.

En la imagen vemos la ventana solicitando el usuario:

![Ventana solicitando el usuario](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Actividad4.3_Captura1.png)

En la siguiente imagen, se nos solicitará la contraseña:

![Ventana solicitando la contraseña](https://bitbucket.org/mplopez-ponte/tareaspsp3/downloads/Actividad4.3_Captura2.png)

Una vez el usuario ya está identificado ya se puede comprobar los directorios y archivos, además de los contenidos de los mismos.
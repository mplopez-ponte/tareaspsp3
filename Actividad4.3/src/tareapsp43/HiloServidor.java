package tareapsp43;

import java.io.*;
import java.net.*;
import java.util.logging.*;
import javax.swing.JOptionPane;

public class HiloServidor extends Thread {
	
	 private int sesionId;
	 private Socket sockCliente;
	 private DataOutputStream salida_flujo;
	 private DataInputStream entrada_flujo;
	 
	 //Creamos los usuarios que vayan a tener acceso al servidor
	 private Usuario usuario1=new Usuario("Paz", "secreta");
	 private Usuario usuario2=new Usuario("Antonio", "pass");
	 
	 //Definimos los archivos según el usuario que haya accedido
	 private File file =new File("\\src\\usuario1\\");
	 private File file1=new File("\\src\\usuario2\\");
	 private File[] directorios;
	 
	 public HiloServidor(Socket sockCliente, int sesionId){
	        this.sockCliente=sockCliente;
	        this.sesionId=sesionId;
	 }
	 
	 public void run(){
	        InputStream is_entrada=null;
	        try {
	            //Abrimos el flujo por donde entrarán las solicitudes
	            is_entrada = sockCliente.getInputStream();
	            //Creamos un buffer y leemos cada línea.
	            BufferedReader bf=new BufferedReader(new InputStreamReader(is_entrada));
	            String nomUsuario=bf.readLine();
	            String password=bf.readLine();
	            
	            //Si el usuario ingresado es el uno, muestra el directorio correspondiente al uno
	            if(nomUsuario.equals(usuario1.getId())&&password.equals(usuario1.getPassword())){
	                try {
	                    OutputStream os_salida=sockCliente.getOutputStream();
	                    salida_flujo=new DataOutputStream(os_salida);
	                    
	                    directorios=file.listFiles();
	                    
	                    for(int i=0;i<directorios.length;i++){
	                        salida_flujo.writeUTF(directorios[i].getName());
	                    }
	                    
	                } catch (IOException ex) {
	                    Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
	                }
	                
	                //Si el usuario ingresado es el dos, muestra el directorio correspondiente al dos
	            }else if (nomUsuario.equals(usuario2.getId())&&password.equals(usuario2.getPassword())){
	                OutputStream salida=null;
	                try {
	                    salida = sockCliente.getOutputStream();
	                    salida_flujo=new DataOutputStream(salida);
	                    directorios=file1.listFiles();
	                    for(int i=0;i<directorios.length;i++){
	                        salida_flujo.writeUTF(directorios[i].getName());
	                    }
	                } catch (IOException ex) {
	                    Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
	                }
	                
	                //En caso de que no exista el usuario introducido se muestra un aviso.
	            }else {
	                System.out.println("Error al acceder. Usuario y/o contraseña incorrectos");
	            }
	        } catch (IOException ex) {
	            Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
	        } finally {
	            try {
	                is_entrada.close();
	            } catch (IOException ex) {
	                Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
	            }
	        }
	        
	    }
	    
	}
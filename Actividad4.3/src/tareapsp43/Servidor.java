package tareapsp43;

import java.io.*;
import java.net.*;
import java.util.logging.*;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Servidor {
	
	//Creando la constante que defina el puerto del Servidor
    private static final int PUERTO=1500;

	public static void main(String[] args) {
		int sesionId;
        ServerSocket sockServidor;
        Socket socket;
        
        try{
        	//Iniciamos el servidor en el puerto
            sockServidor=new ServerSocket(PUERTO);
            socket=sockServidor.accept();
            sesionId=0;
            
            while(true){
            	//Comienza el hilo
                new HiloServidor(socket, sesionId).start();
                sesionId++;
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
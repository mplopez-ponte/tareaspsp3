package tareapsp43;

import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 * 
 * @author María Paz López Ponte
 *
 */

public class Cliente {

	public static void main(String[] args) {
		
		String nomUser;
        String pass;
        Socket socket=null;
        int PUERTO=1500;
        DataInputStream entrada_flujo;
        BufferedWriter bf;

        try{
            socket=new Socket("localhost", PUERTO);
            
        }catch (IOException e){
            System.err.println("No se puede establecer la conexión");
        }
        
        if(socket!=null){
            try{
            nomUser=JOptionPane.showInputDialog("Introducir el nombre de usuario:");
            pass=JOptionPane.showInputDialog("Introducir contraseña:");
            OutputStream salida=socket.getOutputStream();
            
            //Vamos a enviar al servidor los datos introducidos por el usuario
            bf=new BufferedWriter(new OutputStreamWriter(salida));
            bf.write(nomUser);
            bf.write(pass);
          
            //Se reciben los datos de los directorios correspondientes al usuario y se muestran por
            //la consola.
            entrada_flujo=new DataInputStream(socket.getInputStream());
            System.out.println(entrada_flujo.readUTF());
            
            }catch(IOException ex){
                System.err.println("Error de conexión al servidor");
            }
        }
    }
    
}